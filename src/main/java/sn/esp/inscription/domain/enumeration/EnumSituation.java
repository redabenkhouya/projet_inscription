package sn.esp.inscription.domain.enumeration;

/**
 * The EnumSituation enumeration.
 */
public enum EnumSituation {
    Celibataire, MarieSansEnfant, MarieAvecEnfant
}
