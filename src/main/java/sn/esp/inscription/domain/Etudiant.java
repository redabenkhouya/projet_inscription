package sn.esp.inscription.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import sn.esp.inscription.domain.enumeration.EnumSexe;

/**
 * A Etudiant.
 */
@Entity
@Table(name = "etudiant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "etudiant")
public class Etudiant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 6, max = 9)
    @Column(name = "num_identifiant", length = 9, nullable = false, unique = true)
    private String numIdentifiant;

    @NotNull
    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    @NotNull
    @Column(name = "lieu_naissance", nullable = false)
    private String lieuNaissance;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sexe", nullable = false)
    private EnumSexe sexe;

    @NotNull
    @Column(name = "ine", nullable = false)
    private String ine;

    @NotNull
    @Size(min = 7)
    @Column(name = "telephone", nullable = false, unique = true)
    private String telephone;

    @OneToMany(mappedBy = "etudiant")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Inscription> etudiants = new HashSet<>();

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumIdentifiant() {
        return numIdentifiant;
    }

    public Etudiant numIdentifiant(String numIdentifiant) {
        this.numIdentifiant = numIdentifiant;
        return this;
    }

    public void setNumIdentifiant(String numIdentifiant) {
        this.numIdentifiant = numIdentifiant;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public Etudiant dateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
        return this;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public Etudiant lieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
        return this;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public EnumSexe getSexe() {
        return sexe;
    }

    public Etudiant sexe(EnumSexe sexe) {
        this.sexe = sexe;
        return this;
    }

    public void setSexe(EnumSexe sexe) {
        this.sexe = sexe;
    }

    public String getIne() {
        return ine;
    }

    public Etudiant ine(String ine) {
        this.ine = ine;
        return this;
    }

    public void setIne(String ine) {
        this.ine = ine;
    }

    public String getTelephone() {
        return telephone;
    }

    public Etudiant telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Set<Inscription> getEtudiants() {
        return etudiants;
    }

    public Etudiant etudiants(Set<Inscription> inscriptions) {
        this.etudiants = inscriptions;
        return this;
    }

    public Etudiant addEtudiant(Inscription inscription) {
        this.etudiants.add(inscription);
        inscription.setEtudiant(this);
        return this;
    }

    public Etudiant removeEtudiant(Inscription inscription) {
        this.etudiants.remove(inscription);
        inscription.setEtudiant(null);
        return this;
    }

    public void setEtudiants(Set<Inscription> inscriptions) {
        this.etudiants = inscriptions;
    }

    public User getUser() {
        return user;
    }

    public Etudiant user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Etudiant)) {
            return false;
        }
        return id != null && id.equals(((Etudiant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Etudiant{" +
            "id=" + getId() +
            ", numIdentifiant='" + getNumIdentifiant() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", lieuNaissance='" + getLieuNaissance() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", ine='" + getIne() + "'" +
            ", telephone='" + getTelephone() + "'" +
            "}";
    }
}
