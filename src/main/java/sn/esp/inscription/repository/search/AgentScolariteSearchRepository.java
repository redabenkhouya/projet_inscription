package sn.esp.inscription.repository.search;

import sn.esp.inscription.domain.AgentScolarite;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link AgentScolarite} entity.
 */
public interface AgentScolariteSearchRepository extends ElasticsearchRepository<AgentScolarite, Long> {
}
