package sn.esp.inscription.repository.search;

import sn.esp.inscription.domain.Assureur;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Assureur} entity.
 */
public interface AssureurSearchRepository extends ElasticsearchRepository<Assureur, Long> {
}
