package sn.esp.inscription.repository;

import sn.esp.inscription.domain.AgentScolarite;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AgentScolarite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentScolariteRepository extends JpaRepository<AgentScolarite, Long> {
}
