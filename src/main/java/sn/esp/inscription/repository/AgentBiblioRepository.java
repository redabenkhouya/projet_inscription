package sn.esp.inscription.repository;

import sn.esp.inscription.domain.AgentBiblio;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AgentBiblio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentBiblioRepository extends JpaRepository<AgentBiblio, Long> {
}
