package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Inscription;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.Niveau;

import java.util.List;

/**
 * Spring Data  repository for the Inscription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InscriptionRepository extends JpaRepository<Inscription, Long> {
    List<Inscription> findByNiveau(Niveau niveau);
    /**
     * liste des des inscripts d'un niveau
     */
}
