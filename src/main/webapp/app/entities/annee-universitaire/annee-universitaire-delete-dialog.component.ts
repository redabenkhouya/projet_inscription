import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';
import { AnneeUniversitaireService } from './annee-universitaire.service';

@Component({
  templateUrl: './annee-universitaire-delete-dialog.component.html',
})
export class AnneeUniversitaireDeleteDialogComponent {
  anneeUniversitaire?: IAnneeUniversitaire;

  constructor(
    protected anneeUniversitaireService: AnneeUniversitaireService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.anneeUniversitaireService.delete(id).subscribe(() => {
      this.eventManager.broadcast('anneeUniversitaireListModification');
      this.activeModal.close();
    });
  }
}
