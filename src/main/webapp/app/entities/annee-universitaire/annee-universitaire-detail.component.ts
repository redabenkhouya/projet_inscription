import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';

@Component({
  selector: 'jhi-annee-universitaire-detail',
  templateUrl: './annee-universitaire-detail.component.html',
})
export class AnneeUniversitaireDetailComponent implements OnInit {
  anneeUniversitaire: IAnneeUniversitaire | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ anneeUniversitaire }) => (this.anneeUniversitaire = anneeUniversitaire));
  }

  previousState(): void {
    window.history.back();
  }
}
