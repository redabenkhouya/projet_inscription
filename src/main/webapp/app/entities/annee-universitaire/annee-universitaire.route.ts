import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAnneeUniversitaire, AnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';
import { AnneeUniversitaireService } from './annee-universitaire.service';
import { AnneeUniversitaireComponent } from './annee-universitaire.component';
import { AnneeUniversitaireDetailComponent } from './annee-universitaire-detail.component';
import { AnneeUniversitaireUpdateComponent } from './annee-universitaire-update.component';

@Injectable({ providedIn: 'root' })
export class AnneeUniversitaireResolve implements Resolve<IAnneeUniversitaire> {
  constructor(private service: AnneeUniversitaireService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAnneeUniversitaire> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((anneeUniversitaire: HttpResponse<AnneeUniversitaire>) => {
          if (anneeUniversitaire.body) {
            return of(anneeUniversitaire.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AnneeUniversitaire());
  }
}

export const anneeUniversitaireRoute: Routes = [
  {
    path: '',
    component: AnneeUniversitaireComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'inscriptionEspApp.anneeUniversitaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AnneeUniversitaireDetailComponent,
    resolve: {
      anneeUniversitaire: AnneeUniversitaireResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'inscriptionEspApp.anneeUniversitaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AnneeUniversitaireUpdateComponent,
    resolve: {
      anneeUniversitaire: AnneeUniversitaireResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'inscriptionEspApp.anneeUniversitaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AnneeUniversitaireUpdateComponent,
    resolve: {
      anneeUniversitaire: AnneeUniversitaireResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'inscriptionEspApp.anneeUniversitaire.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
