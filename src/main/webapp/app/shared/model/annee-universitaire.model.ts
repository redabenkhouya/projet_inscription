import { IInscription } from 'app/shared/model/inscription.model';

export interface IAnneeUniversitaire {
  id?: number;
  isActive?: boolean;
  libelle?: string;
  anneeUniversitaires?: IInscription[];
}

export class AnneeUniversitaire implements IAnneeUniversitaire {
  constructor(public id?: number, public isActive?: boolean, public libelle?: string, public anneeUniversitaires?: IInscription[]) {
    this.isActive = this.isActive || false;
  }
}
