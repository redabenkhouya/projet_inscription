export const enum EnumSituation {
  Celibataire = 'Celibataire',

  MarieSansEnfant = 'MarieSansEnfant',

  MarieAvecEnfant = 'MarieAvecEnfant',
}
