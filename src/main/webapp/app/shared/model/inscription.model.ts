import { INiveau } from 'app/shared/model/niveau.model';
import { IAnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { EnumSituation } from 'app/shared/model/enumerations/enum-situation.model';

export interface IInscription {
  id?: number;
  estApte?: boolean;
  estBoursier?: boolean;
  estAssure?: boolean;
  enRegleBiblio?: boolean;
  situationMatrimoniale?: EnumSituation;
  niveau?: INiveau;
  anneeUniversitaire?: IAnneeUniversitaire;
  etudiant?: IEtudiant;
}

export class Inscription implements IInscription {
  constructor(
    public id?: number,
    public estApte?: boolean,
    public estBoursier?: boolean,
    public estAssure?: boolean,
    public enRegleBiblio?: boolean,
    public situationMatrimoniale?: EnumSituation,
    public niveau?: INiveau,
    public anneeUniversitaire?: IAnneeUniversitaire,
    public etudiant?: IEtudiant
  ) {
    this.estApte = this.estApte || false;
    this.estBoursier = this.estBoursier || false;
    this.estAssure = this.estAssure || false;
    this.enRegleBiblio = this.enRegleBiblio || false;
  }
}
