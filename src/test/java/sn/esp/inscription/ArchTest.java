package sn.esp.inscription;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.esp.inscription");

        noClasses()
            .that()
                .resideInAnyPackage("sn.esp.inscription.service..")
            .or()
                .resideInAnyPackage("sn.esp.inscription.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..sn.esp.inscription.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
