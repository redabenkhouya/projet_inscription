import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { InscriptionEspTestModule } from '../../../test.module';
import { AgentBiblioComponent } from 'app/entities/agent-biblio/agent-biblio.component';
import { AgentBiblioService } from 'app/entities/agent-biblio/agent-biblio.service';
import { AgentBiblio } from 'app/shared/model/agent-biblio.model';

describe('Component Tests', () => {
  describe('AgentBiblio Management Component', () => {
    let comp: AgentBiblioComponent;
    let fixture: ComponentFixture<AgentBiblioComponent>;
    let service: AgentBiblioService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [InscriptionEspTestModule],
        declarations: [AgentBiblioComponent],
      })
        .overrideTemplate(AgentBiblioComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentBiblioComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentBiblioService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AgentBiblio(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.agentBiblios && comp.agentBiblios[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
