import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { InscriptionEspTestModule } from '../../../test.module';
import { AgentScolariteComponent } from 'app/entities/agent-scolarite/agent-scolarite.component';
import { AgentScolariteService } from 'app/entities/agent-scolarite/agent-scolarite.service';
import { AgentScolarite } from 'app/shared/model/agent-scolarite.model';

describe('Component Tests', () => {
  describe('AgentScolarite Management Component', () => {
    let comp: AgentScolariteComponent;
    let fixture: ComponentFixture<AgentScolariteComponent>;
    let service: AgentScolariteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [InscriptionEspTestModule],
        declarations: [AgentScolariteComponent],
      })
        .overrideTemplate(AgentScolariteComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentScolariteComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentScolariteService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AgentScolarite(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.agentScolarites && comp.agentScolarites[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
