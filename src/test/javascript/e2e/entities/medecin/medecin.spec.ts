import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  MedecinComponentsPage,
  /* MedecinDeleteDialog, */
  MedecinUpdatePage,
} from './medecin.page-object';

const expect = chai.expect;

describe('Medecin e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let medecinComponentsPage: MedecinComponentsPage;
  let medecinUpdatePage: MedecinUpdatePage;
  /* let medecinDeleteDialog: MedecinDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Medecins', async () => {
    await navBarPage.goToEntity('medecin');
    medecinComponentsPage = new MedecinComponentsPage();
    await browser.wait(ec.visibilityOf(medecinComponentsPage.title), 5000);
    expect(await medecinComponentsPage.getTitle()).to.eq('inscriptionEspApp.medecin.home.title');
    await browser.wait(ec.or(ec.visibilityOf(medecinComponentsPage.entities), ec.visibilityOf(medecinComponentsPage.noResult)), 1000);
  });

  it('should load create Medecin page', async () => {
    await medecinComponentsPage.clickOnCreateButton();
    medecinUpdatePage = new MedecinUpdatePage();
    expect(await medecinUpdatePage.getPageTitle()).to.eq('inscriptionEspApp.medecin.home.createOrEditLabel');
    await medecinUpdatePage.cancel();
  });

  /* it('should create and save Medecins', async () => {
        const nbButtonsBeforeCreate = await medecinComponentsPage.countDeleteButtons();

        await medecinComponentsPage.clickOnCreateButton();

        await promise.all([
            medecinUpdatePage.setMatriculeInput('matricule'),
            medecinUpdatePage.userSelectLastOption(),
        ]);

        expect(await medecinUpdatePage.getMatriculeInput()).to.eq('matricule', 'Expected Matricule value to be equals to matricule');

        await medecinUpdatePage.save();
        expect(await medecinUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await medecinComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Medecin', async () => {
        const nbButtonsBeforeDelete = await medecinComponentsPage.countDeleteButtons();
        await medecinComponentsPage.clickOnLastDeleteButton();

        medecinDeleteDialog = new MedecinDeleteDialog();
        expect(await medecinDeleteDialog.getDialogTitle())
            .to.eq('inscriptionEspApp.medecin.delete.question');
        await medecinDeleteDialog.clickOnConfirmButton();

        expect(await medecinComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
