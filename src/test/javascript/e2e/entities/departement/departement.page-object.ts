import { element, by, ElementFinder } from 'protractor';

export class DepartementComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-departement div table .btn-danger'));
  title = element.all(by.css('jhi-departement div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class DepartementUpdatePage {
  pageTitle = element(by.id('jhi-departement-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  codeDeptInput = element(by.id('field_codeDept'));
  libelleLongInput = element(by.id('field_libelleLong'));
  chefInput = element(by.id('field_chef'));
  typeDeptSelect = element(by.id('field_typeDept'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCodeDeptInput(codeDept: string): Promise<void> {
    await this.codeDeptInput.sendKeys(codeDept);
  }

  async getCodeDeptInput(): Promise<string> {
    return await this.codeDeptInput.getAttribute('value');
  }

  async setLibelleLongInput(libelleLong: string): Promise<void> {
    await this.libelleLongInput.sendKeys(libelleLong);
  }

  async getLibelleLongInput(): Promise<string> {
    return await this.libelleLongInput.getAttribute('value');
  }

  async setChefInput(chef: string): Promise<void> {
    await this.chefInput.sendKeys(chef);
  }

  async getChefInput(): Promise<string> {
    return await this.chefInput.getAttribute('value');
  }

  async setTypeDeptSelect(typeDept: string): Promise<void> {
    await this.typeDeptSelect.sendKeys(typeDept);
  }

  async getTypeDeptSelect(): Promise<string> {
    return await this.typeDeptSelect.element(by.css('option:checked')).getText();
  }

  async typeDeptSelectLastOption(): Promise<void> {
    await this.typeDeptSelect.all(by.tagName('option')).last().click();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class DepartementDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-departement-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-departement'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
