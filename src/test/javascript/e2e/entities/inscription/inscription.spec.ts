import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { InscriptionComponentsPage, InscriptionDeleteDialog, InscriptionUpdatePage } from './inscription.page-object';

const expect = chai.expect;

describe('Inscription e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let inscriptionComponentsPage: InscriptionComponentsPage;
  let inscriptionUpdatePage: InscriptionUpdatePage;
  let inscriptionDeleteDialog: InscriptionDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Inscriptions', async () => {
    await navBarPage.goToEntity('inscription');
    inscriptionComponentsPage = new InscriptionComponentsPage();
    await browser.wait(ec.visibilityOf(inscriptionComponentsPage.title), 5000);
    expect(await inscriptionComponentsPage.getTitle()).to.eq('inscriptionEspApp.inscription.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(inscriptionComponentsPage.entities), ec.visibilityOf(inscriptionComponentsPage.noResult)),
      1000
    );
  });

  it('should load create Inscription page', async () => {
    await inscriptionComponentsPage.clickOnCreateButton();
    inscriptionUpdatePage = new InscriptionUpdatePage();
    expect(await inscriptionUpdatePage.getPageTitle()).to.eq('inscriptionEspApp.inscription.home.createOrEditLabel');
    await inscriptionUpdatePage.cancel();
  });

  it('should create and save Inscriptions', async () => {
    const nbButtonsBeforeCreate = await inscriptionComponentsPage.countDeleteButtons();

    await inscriptionComponentsPage.clickOnCreateButton();

    await promise.all([
      inscriptionUpdatePage.situationMatrimonialeSelectLastOption(),
      inscriptionUpdatePage.niveauSelectLastOption(),
      inscriptionUpdatePage.anneeUniversitaireSelectLastOption(),
      inscriptionUpdatePage.etudiantSelectLastOption(),
    ]);

    const selectedEstApte = inscriptionUpdatePage.getEstApteInput();
    if (await selectedEstApte.isSelected()) {
      await inscriptionUpdatePage.getEstApteInput().click();
      expect(await inscriptionUpdatePage.getEstApteInput().isSelected(), 'Expected estApte not to be selected').to.be.false;
    } else {
      await inscriptionUpdatePage.getEstApteInput().click();
      expect(await inscriptionUpdatePage.getEstApteInput().isSelected(), 'Expected estApte to be selected').to.be.true;
    }
    const selectedEstBoursier = inscriptionUpdatePage.getEstBoursierInput();
    if (await selectedEstBoursier.isSelected()) {
      await inscriptionUpdatePage.getEstBoursierInput().click();
      expect(await inscriptionUpdatePage.getEstBoursierInput().isSelected(), 'Expected estBoursier not to be selected').to.be.false;
    } else {
      await inscriptionUpdatePage.getEstBoursierInput().click();
      expect(await inscriptionUpdatePage.getEstBoursierInput().isSelected(), 'Expected estBoursier to be selected').to.be.true;
    }
    const selectedEstAssure = inscriptionUpdatePage.getEstAssureInput();
    if (await selectedEstAssure.isSelected()) {
      await inscriptionUpdatePage.getEstAssureInput().click();
      expect(await inscriptionUpdatePage.getEstAssureInput().isSelected(), 'Expected estAssure not to be selected').to.be.false;
    } else {
      await inscriptionUpdatePage.getEstAssureInput().click();
      expect(await inscriptionUpdatePage.getEstAssureInput().isSelected(), 'Expected estAssure to be selected').to.be.true;
    }
    const selectedEnRegleBiblio = inscriptionUpdatePage.getEnRegleBiblioInput();
    if (await selectedEnRegleBiblio.isSelected()) {
      await inscriptionUpdatePage.getEnRegleBiblioInput().click();
      expect(await inscriptionUpdatePage.getEnRegleBiblioInput().isSelected(), 'Expected enRegleBiblio not to be selected').to.be.false;
    } else {
      await inscriptionUpdatePage.getEnRegleBiblioInput().click();
      expect(await inscriptionUpdatePage.getEnRegleBiblioInput().isSelected(), 'Expected enRegleBiblio to be selected').to.be.true;
    }

    await inscriptionUpdatePage.save();
    expect(await inscriptionUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await inscriptionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Inscription', async () => {
    const nbButtonsBeforeDelete = await inscriptionComponentsPage.countDeleteButtons();
    await inscriptionComponentsPage.clickOnLastDeleteButton();

    inscriptionDeleteDialog = new InscriptionDeleteDialog();
    expect(await inscriptionDeleteDialog.getDialogTitle()).to.eq('inscriptionEspApp.inscription.delete.question');
    await inscriptionDeleteDialog.clickOnConfirmButton();

    expect(await inscriptionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
