FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/*.jar app.jar
ENTRYPOINT ["java","-Djava.Security.egd=file:/dev/./urandom","-jar","/app.jar"]
